jQuery(function($) {
	//menu toggle
	$('#menu-toggle').on('click', function(){
		$(this).toggleClass('active');
		$('body').toggleClass("showmenu");
	});

	// mobile menu
	var mobileMenu = $('.mobile-menu .menu');
	mobileMenu.children('li').each(function() {
		if($(this).hasClass('menu-item-has-children')) {
			$(this).prepend('<span class="arrowCtl"><em class="fa fa-chevron-down"></em></span>');
		}
	});
	var aCtrl = $('.arrowCtl');
	aCtrl.on('click', function() {
		var subMenu = $(this).siblings('ul');
		if(subMenu.is(':hidden')) {
			subMenu.slideDown('400');
			$(this).children('em').removeClass('fa fa-chevron-down').addClass('fa fa-chevron-up');
		} else {
			subMenu.slideUp('400');
			$(this).children('em').removeClass('fa fa-chevron-up').addClass('fa fa-chevron-down');
		}
	});	
	$('.flex-control-nav').each(function() {
		console.log(12312312);
		$(this).flexslider({
			animation: "slide",
        customDirectionNav: $(".nav-carousel a"),
        controlNav: true,
        animationLoop: true,
        slideshow: true,
        itemWidth: 220,
        itemMargin: 10,
        minItems: 1,
        maxItems: 4

		});
	});

});


	