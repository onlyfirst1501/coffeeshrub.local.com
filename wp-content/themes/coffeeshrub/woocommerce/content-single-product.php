<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     4.0.0
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly
?>

<?php
/**
 * woocommerce_before_single_product hook
 *
 * @hooked wc_print_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form();
    return;
}
global $post, $product;

$short_description = apply_filters('woocommerce_short_description', $post->post_excerpt);

if (!$short_description) {
    return;
}

?>
<?php
$entry_image_col = 'col-md-7';
$entry_summary = 'col-md-5';
?>
<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="row summary-container" id="product-detail">
        <div class="<?php echo esc_attr($entry_image_col) ?> col-sm-5 ">
            <div class="single-product-images">
                <?php
                /**
                 * woocommerce_before_single_product_summary hook
                 *
                 * @hooked woocommerce_show_product_sale_flash - 10
                 * @hooked woocommerce_show_product_images - 20
                 */
                do_action('woocommerce_before_single_product_summary');
                ?>
            </div>
        </div>
        <div class="<?php echo esc_attr($entry_summary) ?> col-sm-7 entry-summary">
            <div class="content">
                <h1 class="product_title entry-title"><?php the_title() ?></h1>
                
                <div class="clearfix"></div>
                 <!-- Code description  -->
                <div class="short-description">
                    <?php echo $short_description; // WPCS: XSS ok. ?>
                </div>
                 <!-- End Code description  -->
                <div class="clearfix"></div>
                <!-- Code Price  -->
                <div class="product-info-price">
                    <div class="price-box price-final_price" data-role="priceBox" data-product-id="<?php echo $product->get_id(); ?>">
                        <p class="price"><?php echo $product->get_price_html(); ?></p>
                    </div>
                    <div class="product-info-stock-sku">
                        <?php echo wc_get_product_category_list($product->get_id(), ', ', '<span class="posted_in">' . _n('Category:', 'Categories:', count($product->get_category_ids()), 'woocommerce') . ' ', '</span>'); ?>
                        <?php do_action('woocommerce_product_meta_end'); ?>
                    </div>
                </div>
                <!-- End Code Price  -->
                <div class="clearfix"></div>
                <!--Code Quantity and button add to cart  -->
                <div class="product-add-form">
                    <?php if ( $product->is_in_stock() ) : ?>

                        <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
            
                        <form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
                            <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
                            <div class="qty">Quantity: </div>
                            <?php
                            do_action( 'woocommerce_before_add_to_cart_quantity' );
                    
                            woocommerce_quantity_input( array(
                                'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                                'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                                'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
                            ) );
                    
                            do_action( 'woocommerce_after_add_to_cart_quantity' );
                            ?>
                            <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
                            <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
                        </form>
                        <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
                    <?php endif; ?>
                    </div>
                <!--End Code Quantity and button add to cart  -->
                <div class="clearfix"></div>
            </div><!-- .summary -->
        </div>
    </div>
    <div class="row summary-container detailer">
        <div class="column col-md-12">
            <?php the_content(); ?>
        </div>
    </div>
    <?php do_action('woocommerce_after_single_product'); ?>
