<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
	<link rel="icon" href="<?php echo get_template_directory_uri() ?>/favicon.ico" type="image/x-icon" sizes="16x16">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="mobile-menu">
		<?php
			wp_nav_menu( array(
				'theme_location' => 'mobile-menu',
				'menu_class'     => 'menu',
			) );
		?>
		<ul class="box-lang box-lang-mobile">
			<?php pll_the_languages( array( 'show_flags' => 0,'show_names' => 1, 'show_code' => 1 ) ); ?>
		</ul>
	</div>
	<div class="mainsite">
		<header class="site-header">
			<div class="container">
				<div class="header-left">
					<?php if ( has_nav_menu( 'primary' )) : ?>
						<button id="menu-toggle" class="menu-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
					<?php endif; ?>
					<div class="site-branding">
							<?php coffeeshrub_the_custom_logo(); ?>
						</div><!-- .site-branding -->
				</div>
				<div class="header-right">
					<div class="hr-top">
						<div class="custom-site-title">
							<a href="<?php echo get_home_url(); ?>" title="<?php echo get_bloginfo(); ?>"><?php echo get_bloginfo(); ?></a>
						</div>

						<div class="box-lang">
							<ul>
							<?php pll_the_languages( array( 'show_flags' => 0,'show_names' => 1 ) ); ?>
							</ul>
						</div>						
						<?php //do_shortcode( '[nc_ajax_cart]');?>
						<?php storefront_header_cart(); ?>						
						<div class="login-box">

								<?php if ( !is_user_logged_in() ) { ?>
								<ul>
									<li><a href="<?php echo wp_login_url(); ?>" title="<?php echo __('Login', 'coffeeshrub') ?>"><?php echo __('Login', 'coffeeshrub') ?></a></li>
									<li><a href="<?php echo wp_registration_url(); ?>" title="<?php echo __('Register', 'coffeeshrub') ?>"><?php echo __('Register', 'coffeeshrub') ?></a></li>
								</ul>
								<?php } else { ?>
								<ul>
									<li><a href="<?php echo get_permalink(32);?>" title="<?php echo __('Your account', 'coffeeshrub') ?>"><em class="fa fa-users"></em>Your account</a></li>
									<li><a href="<?php echo wp_logout_url(); ?>" title="<?php echo __('Sign out', 'coffeeshrub') ?>"><em class="fa fa-sign-out"></em><?php echo __('Sign out', 'coffeeshrub') ?></a></li>
								</ul>
								<?php } ?>

						</div>
					</div>
					<div class="site-nav">
						<?php if ( has_nav_menu( 'primary' )) : ?>
							<div id="site-header-menu" class="site-header-menu">
								<?php if ( has_nav_menu( 'primary' ) ) : ?>
									<div id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
										<?php
										wp_nav_menu( array(
											'theme_location' => 'primary',
											'menu_class'     => 'primary-menu',
										) );
										?>
									</div><!-- .main-navigation -->
								<?php endif; ?>
							</div><!-- .site-header-menu -->
						<?php endif; ?>
						<?php storefront_product_search(); ?>
					</div>
				</div>
			</div>
		</header><!-- .site-header -->

		<?php if ( !is_front_page() ): ?>
			<div class="breadcrumb">
				<div class="container">
					<?php get_breadcrumb(); ?>
				</div>
			</div>
		<?php endif;	?>

