<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */

get_header(); ?>

<main id="main" class="site-main">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="primary-area search-area">
					<?php if ( have_posts() ) : ?>

					<div class="page-header">
						<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'coffeeshrub' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
					</div><!-- .page-header -->
					 <div class="search-list news-list">
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'search' );
					endwhile; ?>
					</div>
					<?php
					// Previous/next page navigation.
					the_posts_pagination( array(
						'prev_text'          => __( 'Previous page', 'coffeeshrub' ),
						'next_text'          => __( 'Next page', 'coffeeshrub' ),
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'coffeeshrub' ) . ' </span>',
					) );

				// If no content, include the "No posts found" template.
				else :
					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>
				</div><!-- .primary-area -->
			</div>
			<div class="col-md-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</main><!-- .site-main -->

<?php get_footer(); ?>
