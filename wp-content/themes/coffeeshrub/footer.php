			<?php get_sidebar( 'content-bottom' ); ?>	
			<section id="footer-top" class="footer top">
				<div class="container">
						<div class="footer_links">
							<div class="row">
								<div class="col-sm-4 bottom_block1">
									<?php the_widget( 'sidebar-1' ); ?> 
								</div>
								<div class="col-sm-4 bottom_block2">
									<?php the_widget( 'sidebar-2' ); ?> 
								</div>
								<div class="col-sm-4 bottom_block3">
									<?php the_widget( 'sidebar-3' ); ?> 
								</div>
							</div>
						</div>
						<div class="footer_visit bottom_block4">
							<?php the_widget( 'sidebar-4' ); ?> 
						</div>
					
				</div>
			</section>	
			<section id="footer-content" class="footer-content" role="complementary">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<?php if( get_theme_mod('site_facebook') || get_theme_mod('site_twitter') || get_theme_mod('site_google') ): ?>
							<div class="social-box">
								<h3><?php echo __('Stay In Touch', 'coffeeshrub') ?></h3>
								<ul>
									<?php if(get_theme_mod('site_facebook')): ?>
										<li><a class="link-fb" href="<?php echo get_theme_mod('site_facebook');?>" title="Facebook" target="_blank"><em class="m2icon m2icon-facebook"></em></a></li>
									<?php endif; ?>
									<?php if(get_theme_mod('site_twitter')): ?>
										<li><a class="link-tw" href="<?php echo get_theme_mod('site_twitter');?>" title="Twitter" target="_blank"><em class="m2icon m2icon-twitter"></em></a></li>
									<?php endif; ?>
									<?php if(get_theme_mod('site_youtube')): ?>
										<li><a class="link-yt" href="<?php echo get_theme_mod('site_youtube');?>" title="Youtube" target="_blank"><em class="m2icon m2icon-youtube"></em></a></li>
									<?php endif; ?>
									<?php if(get_theme_mod('site_instagram')): ?>
										<li><a class="link-yt" href="<?php echo get_theme_mod('site_instagram');?>" title="Instagram" target="_blank"><em class="m2icon m2icon-instagram"></em></a></li>
									<?php endif; ?>
								</ul>
							</div>
							<?php endif; ?>
						</div>
						<div class="col-sm-6">
						</div>
					</div>
				</div>
			</section><!-- .footer-top -->	
			<footer class="site-footer">
				<div class="container">						
				<?php
					/**
					 * Fires before the coffeeshrub footer text for footer customization.
					 *
					 * @since coffeeshrub 1.0
					 */
					do_action( 'coffeeshrub_credits' );
				?>
				<?php if(get_theme_mod( 'site_copyright' )): ?>
					<div class="copyright"><?php echo get_theme_mod( 'site_copyright' ); ?></div>
				<?php endif; ?>										
				</div>
			</footer><!-- /.site-footer -->

		</div><!-- /.mainsite -->
		<?php wp_footer(); ?>
	</body>
</html>
