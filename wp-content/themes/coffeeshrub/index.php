<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since Base themes 1.0
 */

  get_header(); ?>
  <main id="main" class="site-main" role="main">
    <div class="container">
      <div class="post-list-wrapper">
        <?php if ( have_posts() ) : ?>
          <?php
          // Start the loop.
          while ( have_posts() ) : the_post();
            get_template_part( 'template-parts/content', get_post_format() );
          // End the loop.
          endwhile;

            // Previous/next page navigation.
            the_posts_pagination( array(
              'prev_text'          => __( 'Previous page', 'coffeeshrub' ),
              'next_text'          => __( 'Next page', 'coffeeshrub' ),
              'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'coffeeshrub' ) . ' </span>',
            ) );

          // If no content, include the "No posts found" template.
          else :
            get_template_part( 'template-parts/content', 'none' );

          endif;
        ?>
        </ol>
      </div>
  </main>
  <?php get_footer(); ?>
