<?php
/**
 * Custom coffeeshrub template tags
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */

if ( ! function_exists( 'coffeeshrub_entry_meta' ) ) :
/**
 * Prints HTML with meta information for the categories, tags.
 *
 * Create your own coffeeshrub_entry_meta() function to override in a child theme.
 *
 * @since coffeeshrub 1.0
 */
function coffeeshrub_entry_meta() {
	$format = get_post_format();
	echo '<div class="entry_meta"><ul>';
	if ( current_theme_supports( 'post-formats', $format ) ) {
		echo '<li>';
		printf( '<span class="entry-format">%1$s<a href="%2$s">%3$s</a></span>',
			sprintf( '<span class="screen-reader-text">%s </span>', _x( 'Format', 'Used before post format.', 'coffeeshrub' ) ),
			esc_url( get_post_format_link( $format ) ),
			get_post_format_string( $format )
		);
		echo '</li>';
	}

	if ( in_array( get_post_type(), array( 'post', 'attachment', 'aocuoi', 'dich-vu' , 'makeup' , 'bang-gia', 'hinh-anh' ) ) ) {
		echo '<li><em class="fa fa-clock-o"></em>'.get_the_date() . '</li>';
	}

	if ( in_array( get_post_type(), array( 'post', 'attachment', 'aocuoi', 'dich-vu', 'makeup' , 'bang-gia', 'hinh-anh' ) ) ) {
		echo '<li>';
		$author_avatar_size = apply_filters( 'coffeeshrub_author_avatar_size', 49 );
		printf( '<span class="byline"><span class="author vcard">%1$s<span class="lb">%2$s </span> <a class="url fn n" href="%3$s">%4$s</a></span></span>',
			'',
			_x( 'Đăng bởi', 'Used before post author name.', 'coffeeshrub' ),
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			get_the_author()
		);
		echo '</li>';
	}


	if (in_array( get_post_type(), array( 'post' ) ) ) {
		echo '<li><span class="lb">Danh mục:</span>';
		coffeeshrub_entry_taxonomies();
		echo '</li>';
	}

	if (in_array( get_post_type(), array('makeup' ) ) ) {
		echo '<li><span class="lb">Danh mục:</span>';
		echo get_the_term_list( get_the_ID(), 'trang-diem', '', ', ' );
		echo '</li>';
	}

	if ( ! is_singular() && ! post_password_required() && ( comments_open() || get_comments_number() ) && 'post' === get_post_type()) {
		echo '<li>';
		echo '<span class="comments-link">';
		comments_popup_link( sprintf( __( 'Leave a comment<span class="screen-reader-text"> on %s</span>', 'coffeeshrub' ), get_the_title() ) );
		echo '</span>';
		echo '</li>';
	}
	if ( 'post' === get_post_type() ) {
		echo '<li><em class="fa fa-comments-o"></em>';
		echo '<a href="<?php the_permalink(); ?>#comments">'.comments_number('No Comments', '1 Comment', '% Comments').'</a>';
		echo '</li>';
	}
	echo '</ul></div>';
}
endif;

if ( ! function_exists( 'coffeeshrub_entry_date' ) ) :
/**
 * Prints HTML with date information for current post.
 *
 * Create your own coffeeshrub_entry_date() function to override in a child theme.
 *
 * @since coffeeshrub 1.0
 */
function coffeeshrub_entry_date() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		get_the_date(),
		esc_attr( get_the_modified_date( 'c' ) ),
		get_the_modified_date()
	);

	printf( '<span class="posted-on"><span class="screen-reader-text">%1$s </span><a href="%2$s" rel="bookmark">%3$s</a></span>',
		_x( 'Posted on', 'Used before publish date.', 'coffeeshrub' ),
		esc_url( get_permalink() ),
		$time_string
	);
}
endif;

if ( ! function_exists( 'coffeeshrub_entry_taxonomies' ) ) :
/**
 * Prints HTML with category and tags for current post.
 *
 * Create your own coffeeshrub_entry_taxonomies() function to override in a child theme.
 *
 * @since coffeeshrub 1.0
 */
function coffeeshrub_entry_taxonomies() {
	$categories_list = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'coffeeshrub' ) );
	if ( $categories_list && coffeeshrub_categorized_blog() ) {
		printf( '<span class="cat-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
			_x( 'Categories', 'Used before category names.', 'coffeeshrub' ),
			$categories_list
		);
	}

	$tags_list = get_the_tag_list( '', _x( ', ', 'Used between list items, there is a space after the comma.', 'coffeeshrub' ) );
	if ( $tags_list ) {
		printf( '<span class="tags-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
			_x( 'Tags', 'Used before tag names.', 'coffeeshrub' ),
			$tags_list
		);
	}
}
endif;

if ( ! function_exists( 'coffeeshrub_post_thumbnail' ) ) :
/**
 * Displays an optional post thumbnail.
 *
 * Wraps the post thumbnail in an anchor element on index views, or a div
 * element when on single views.
 *
 * Create your own coffeeshrub_post_thumbnail() function to override in a child theme.
 *
 * @since coffeeshrub 1.0
 */
function coffeeshrub_post_thumbnail() {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	if ( is_singular() ) :
	?>

	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div><!-- .post-thumbnail -->

	<?php else : ?>

	<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		<?php the_post_thumbnail( 'post-thumbnail', array( 'alt' => the_title_attribute( 'echo=0' ) ) ); ?>
	</a>

	<?php endif; // End is_singular()
}
endif;

if ( ! function_exists( 'coffeeshrub_excerpt' ) ) :
	/**
	 * Displays the optional excerpt.
	 *
	 * Wraps the excerpt in a div element.
	 *
	 * Create your own coffeeshrub_excerpt() function to override in a child theme.
	 *
	 * @since coffeeshrub 1.0
	 *
	 * @param string $class Optional. Class string of the div element. Defaults to 'entry-summary'.
	 */
	function coffeeshrub_excerpt( $class = 'entry-summary' ) {
		$class = esc_attr( $class );

		if ( has_excerpt() || is_search() ) : ?>
			<div class="<?php echo $class; ?>">
				<?php the_excerpt(); ?>
			</div><!-- .<?php echo $class; ?> -->
		<?php endif;
	}
endif;

if ( ! function_exists( 'coffeeshrub_excerpt_more' ) && ! is_admin() ) :
/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * Create your own coffeeshrub_excerpt_more() function to override in a child theme.
 *
 * @since coffeeshrub 1.0
 *
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function coffeeshrub_excerpt_more() {
	$link = sprintf( '<a href="%1$s" class="more-link">%2$s</a>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'coffeeshrub' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'coffeeshrub_excerpt_more' );
endif;

if ( ! function_exists( 'coffeeshrub_categorized_blog' ) ) :
/**
 * Determines whether blog/site has more than one category.
 *
 * Create your own coffeeshrub_categorized_blog() function to override in a child theme.
 *
 * @since coffeeshrub 1.0
 *
 * @return bool True if there is more than one category, false otherwise.
 */
function coffeeshrub_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'coffeeshrub_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'coffeeshrub_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so coffeeshrub_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so coffeeshrub_categorized_blog should return false.
		return false;
	}
}
endif;

/**
 * Flushes out the transients used in coffeeshrub_categorized_blog().
 *
 * @since coffeeshrub 1.0
 */
function coffeeshrub_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'coffeeshrub_categories' );
}
add_action( 'edit_category', 'coffeeshrub_category_transient_flusher' );
add_action( 'save_post',     'coffeeshrub_category_transient_flusher' );

if ( ! function_exists( 'coffeeshrub_the_custom_logo' ) ) :
/**
 * Displays the optional custom logo.
 *
 * Does nothing if the custom logo is not available.
 *
 * @since coffeeshrub 1.2
 */
function coffeeshrub_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}
endif;

function getBannerTop() {
	$banner_url = get_theme_mod( 'coffeeshrub_banner_url' );
	$banner_img = get_theme_mod( 'coffeeshrub_banner_image' );
	$image_alt = image_alt_by_url($banner_img);
	if($banner_url) {
		echo '<a href="'.$banner_url.'" title="'.$image_alt.'" rel="home">';
	}
		echo '<img src="'.$banner_img.'" alt="'.$image_alt.'" />';
	if($banner_url) { echo '</a>'; }
}

function image_alt_by_url( $image_url ) {
    global $wpdb;

    if( empty( $image_url ) ) {
        return false;
    }

    $query_arr  = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->posts} WHERE guid='%s';", strtolower( $image_url ) ) );
    $image_id   = ( ! empty( $query_arr ) ) ? $query_arr[0] : 0;

    return get_post_meta( $image_id, '_wp_attachment_image_alt', true );
}

