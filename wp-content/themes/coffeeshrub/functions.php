<?php
/**
 * News Express functions and definitions
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */

/**
 * News Express only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
  require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'coffeeshrub_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own coffeeshrub_setup() function to override in a child theme.
 *
 * @since coffeeshrub 1.0
 */
function coffeeshrub_setup() {
  /*
   * Make theme available for translation.
   * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/coffeeshrub
   * If you're building a theme based on News Express, use a find and replace
   * to change 'coffeeshrub' to the name of your theme in all the template files
   */
  load_theme_textdomain( 'coffeeshrub' );

  // Add default posts and comments RSS feed links to head.
  add_theme_support( 'automatic-feed-links' );

  /*
   * Let WordPress manage the document title.
   * By adding theme support, we declare that this theme does not use a
   * hard-coded <title> tag in the document head, and expect WordPress to
   * provide it for us.
   */
  add_theme_support( 'title-tag' );

  /*
   * Enable support for custom logo.
   *
   *  @since coffeeshrub 1.0
   */
  add_theme_support( 'custom-logo', array(
    'height'      => 240,
    'width'       => 240,
    'flex-height' => true,
  ) );

  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
   */
  add_theme_support( 'post-thumbnails' );
  set_post_thumbnail_size( 1200, 9999 );

  add_image_size( 'xsmall', 80, 50, true );
  add_image_size( 'xmedium', 594, 337, true );
  add_image_size( 'archive-size', 200, 110, true );

  // This theme uses wp_nav_menu() in two locations.
  register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'coffeeshrub' ),
    'mobile-menu'  => __( 'Mobile Menu', 'coffeeshrub' ),
  ) );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ) );

  /*
   * Enable support for Post Formats.
   *
   * See: https://codex.wordpress.org/Post_Formats
   */
  add_theme_support( 'post-formats', array(
    'aside',
    'image',
    'video',
    'quote',
    'link',
    'gallery',
    'status',
    'audio',
    'chat',
  ) );


  // Indicate widget sidebars can use selective refresh in the Customizer.
  //add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // coffeeshrub_setup
add_action( 'after_setup_theme', 'coffeeshrub_setup' );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since coffeeshrub 1.0
 */
function coffeeshrub_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Sidebar', 'coffeeshrub' ),
    'id'            => 'sidebar-1',
    'description'   => __( 'Add widgets here to appear in right on site.', 'coffeeshrub' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
    'name'          => __( 'Bottom block 1', 'coffeeshrub' ),
    'id'            => 'sidebar-2',
    'description'   => __( 'Add widgets here to appear in bottom of site.', 'coffeeshrub' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
    'name'          => __( 'Bottom block 2', 'coffeeshrub' ),
    'id'            => 'sidebar-3',
    'description'   => __( 'Add widgets here to appear in bottom of site.', 'coffeeshrub' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
    'name'          => __( 'Bottom block 3', 'coffeeshrub' ),
    'id'            => 'sidebar-4',
    'description'   => __( 'Add widgets here to appear in bottom of site.', 'coffeeshrub' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
    'name'          => __( 'Bottom block 4', 'coffeeshrub' ),
    'id'            => 'sidebar-5',
    'description'   => __( 'Add widgets here to appear in bottom of site.', 'coffeeshrub' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'coffeeshrub_widgets_init' );

/**
 * Enqueues scripts and styles.
 *
 * @since coffeeshrub 1.0
 */
function coffeeshrub_scripts()
{
    // Theme stylesheet.
    wp_enqueue_style('coffeeshrub-libs', get_template_directory_uri() . '/css/libs.css', array(), '20160815');
    wp_enqueue_style('coffeeshrub-style', get_stylesheet_uri());

    // jQuery move to bottom
    // wp_scripts()->add_data('jquery', 'group', 1);
    // wp_scripts()->add_data('jquery-core', 'group', 1);
    // wp_scripts()->add_data('jquery-migrate', 'group', 1);

    // Load the html5 shiv.
    wp_enqueue_script('jquery-html5', get_template_directory_uri() . '/js/html5.js', array('jquery'), '3.7.3');
    wp_script_add_data('jquery-html5', 'conditional', 'lt IE 9');

    wp_enqueue_script('jquery-respond', get_template_directory_uri() . '/js/respond.min.js', array('jquery'), '1.4.2', true);
    wp_script_add_data('jquery-respond', 'conditional', 'lt IE 9');

    wp_enqueue_script('jquery-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array('jquery'), '20160816', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_register_script('modernizr-script', get_template_directory_uri() . '/js/modernizr.js', array('jquery'), '1', true);
    wp_enqueue_script('modernizr-script');

    wp_register_script('site-script', get_template_directory_uri() . '/js/site.js', array('jquery'), '1', true);
    wp_enqueue_script('site-script');
}

add_action('wp_enqueue_scripts', 'coffeeshrub_scripts');

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/breadcrumb.php';
require get_template_directory() . '/inc/woocommerce.php';


// Change admin bar style
add_action('get_header', 'coffeeshrub_filter_head');

function coffeeshrub_filter_head() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

// fix custom panigation error
function wpa_cpt_tags( $query ) {
  if ( $query->is_tag() && $query->is_main_query() ) {
    $query->set( 'post_type', array( 'post', 'bussiness' ) );
  }
}
//add_action( 'pre_get_posts', 'wpa_cpt_tags' ); // Use when you add new custom post type



// Update WooCommerce Flexslider options

add_filter( 'woocommerce_single_product_carousel_options', 'ud_update_woo_flexslider_options' );

function ud_update_woo_flexslider_options( $options ) {

    $options['directionNav'] = true;
    $options['sync'] = '.flex-control-thumbs';

    return $options;
}
add_action( 'after_setup_theme', 'remove_woo_features', 999 );
function remove_woo_features(){
remove_theme_support( 'wc-product-gallery-slider' );
}