<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */
/*
Template Name: Page - Archive
*/

get_header(); ?>

<main id="main" class="site-main" role="main">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-md-9">
        	<h1 class="screen-reader-text"><?php wp_title(''); ?></h1>
					<?php
					$args = array(
					    'post_type'      => 'page',
					    'posts_per_page' => -1,
					    'post_parent'    => $post->ID,
					    'order'          => 'ASC',
					    'orderby'        => 'menu_order'
					 );

					$parent = new WP_Query( $args );
					if ( $parent->have_posts() ) : ?>
				    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
			        <div id="parent-<?php the_ID(); ?>" class="page-item">
		            <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
		            <p><?php the_excerpt(); ?></p>
			        </div>
				    <?php endwhile; ?>
					<?php endif; wp_reset_postdata(); ?>
				</div>
				<div class="col-sm-4 col-md-3">
          <?php get_sidebar(); ?>
        </div>
       </div>
		</div>
	</main><!-- .site-main -->

<?php get_footer(); ?>
