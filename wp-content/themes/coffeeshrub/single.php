<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */

get_header(); ?>

<main id="main" class="site-main">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="sinlge-content">
			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				// Include the single post content template.
				get_template_part( 'template-parts/content', 'single' );

				// End of the loop.
			endwhile;
			?>
				 </div><!-- .sinlge-content -->

				<?php
					// related posts
					$categories = get_the_category($post->ID);
					if ($categories) {
							$category_ids = array();
							foreach($categories as $individual_category) { $category_ids[] = $individual_category->term_id; }
							$args=array(
								'category__in' => $category_ids,
								'post__not_in' => array($post->ID),
								'showposts'=>20,
							);
							$my_query = new wp_query($args);

						if($my_query->have_posts()) {
						?>
							<div class="related-posts">
								<h2 class="headline"><?php _e("Các tin khác","coffeeshrub"); ?></h2>
								<ul class="clearfix">
								<?php while ($my_query->have_posts()) {
									$my_query->the_post(); ?>
									<li><a href="<?php the_permalink() ?>" title="<?php echo get_the_title(); ?>"><?php echo get_the_title(); ?></a></li>
								<?php }; ?>
								</ul>
							</div>
					<?php };
					};
					wp_reset_query();
				?>
			</div>
		</div>
	</div>
</main><!-- .site-main -->

<?php get_footer(); ?>
