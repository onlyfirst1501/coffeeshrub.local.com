<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */

get_header(); ?>

<main id="main" class="site-main" role="main">
<div class="container">
	<section class="error-404 not-found">
		<p class="error-nb">404</p>
		<h1 class="page-title"><?php _e( 'Oh! Không thể tìm thấy trang này.', 'teecoin' ); ?></h1>
	</section><!-- .error-404 -->
	</div>
</main><!-- .site-main -->
<?php get_footer(); ?>
