<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */
/*
Template Name: Page - home - en
*/
get_header(); ?>
<main id="main" class="site-main">
	<div class="container">
		<div class="primary-area">			
			<h1 class="screen-reader-text"><?php echo get_bloginfo(); ?></h1>
			<?php 
				$show_home_slider = get_field( 'show_home_slider' ); 
				$home_short_code = get_field( 'home_short_code' );
				$home_info_code = get_field('home_info_code');
				if ( $show_home_slider ) {
            ?>
            <div class="home-slider">
            	<?php echo do_shortcode(''.$home_short_code.''); ?>
			</div>
			<div class="home-intro">
				<?php echo do_shortcode(''.$home_info_code.''); ?>
			</div>
			<?php }?>
			<h2>Best Sale Product Coffee</h2>
			<?php echo do_shortcode('[products limit="8" columns="4" best_selling="true" ]')?>	      		      	
			<?php
				// Start the loop.
				while ( have_posts() ) : the_post();

				// Include the page content template.
				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
				// End of the loop.
				endwhile;
			?>
		</div>
	</div>
</main><!-- .site-main -->
<?php get_footer(); ?>
