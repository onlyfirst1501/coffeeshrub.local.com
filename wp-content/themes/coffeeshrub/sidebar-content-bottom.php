<?php
/**
 * The template for the content bottom widget areas on posts and pages
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */

if ( ! is_active_sidebar( 'sidebar-2' ) && ! is_active_sidebar( 'sidebar-3' ) && ! is_active_sidebar( 'sidebar-4' ) && ! is_active_sidebar( 'sidebar-5' ) ) {
	return;
}

// If we get this far, we have widgets. Let's do this.
?>
<section id="site-bottom" class="site-bottom" role="complementary">
	<div class="container bg-content">
		<div class="footer_links">
			<div class="row">
				<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="widget-area">
							<?php dynamic_sidebar( 'sidebar-2' ); ?>
						</div><!-- .widget-area -->
					</div>
				<?php endif; ?>

				<?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="widget-area">
							<?php dynamic_sidebar( 'sidebar-3' ); ?>
						</div><!-- .widget-area -->
					</div>
				<?php endif; ?>
				<?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="widget-area">
							<?php dynamic_sidebar( 'sidebar-4' ); ?>
						</div><!-- .widget-area -->
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="footer_visit bottom_block4">
			<div class="row">
				<?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
					<div class="col-md-12 col-sm-12">
						<div class="widget-area">
							<?php dynamic_sidebar( 'sidebar-5' ); ?>
						</div><!-- .widget-area -->
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section><!-- .content-bottom-widgets -->
