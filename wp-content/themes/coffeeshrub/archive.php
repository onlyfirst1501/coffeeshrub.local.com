<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since coffeeshrub 1.0
 */

get_header(); ?>

<main id="main" class="site-main">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="primary-area">
					<?php if ( have_posts() ) : ?>
					<?php get_breadcrumb(); ?>
					<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
					<?php
					// Start the Loop.
					echo '<div class="news-list">';
					while ( have_posts() ) : the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content-loop', get_post_format() );

					// End the loop.
					endwhile;
					echo '</div>';
					the_posts_pagination( array(
					 'prev_text' => __( '&#8249;'),
    				'next_text' => __( '&#8250;'),
    				'type' => 'plain'
				) );
				// If no content, include the "No posts found" template.
				else :
					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>
				</div><!-- .primary-area -->
			</div>
			<div class="col-md-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</main><!-- .site-main -->

<?php get_footer(); ?>

