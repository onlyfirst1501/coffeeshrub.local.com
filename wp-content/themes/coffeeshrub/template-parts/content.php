<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */
?>

<div id="post-<?php the_ID(); ?>" class="item-post row">
	<div class="col-md-4">
		<?php echo get_the_post_thumbnail( $post_id, 'large','style=height:auto;' ); ?>
	</div>
	<div class="col-md-8">
	<div class="post-content">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php coffeeshrub_excerpt(); ?>
		<div class="entry-content">
		<?php
			echo $excerpt = wp_trim_words( get_the_content(), 100,'...');
		?>
		<br>
		<a href="<?php the_permalink() ?>" class="pull-right"> More Link</a>
	</div><!-- .entry-content -->
	</div>
	</div>
</div>