<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post-thumbnail">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
		</a>
	</div><!-- .post-thumbnail -->
	<div class="entry-content post-single">
		<?php
			the_content();
		?>
	</div><!-- .entry-content -->


</article><!-- #post-## -->
