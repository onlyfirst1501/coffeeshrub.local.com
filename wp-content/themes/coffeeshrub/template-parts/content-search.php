<?php
/**
 * The template part for displaying results in search pages
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */
?>

<article class="news-item" id="post-<?php the_ID(); ?>">
	<a href="<?php echo esc_url( get_permalink() ); ?>" class="img">
  	<?php echo get_the_post_thumbnail( get_the_ID(), 'archive-size' ); ?>
  </a>
  <div class="content">
    <h3 class="title"><a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title(); ?>"><?php the_title(); ?><?php if ( is_sticky() ) : ?>
      <span class="sticky-post">&nbsp;</span>
    <?php endif; ?></a></h3>
    <?php
    if ( has_excerpt()) : ?>
      <div class="excerpt">
        <?php the_excerpt(); ?>
      </div>
    <?php endif;
    ?>
  </div>

	<?php if ( 'post' === get_post_type() ) : ?>

		<div class="entry-footer">
			<?php // coffeeshrub_entry_meta(); ?>
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'coffeeshrub' ),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</div><!-- .entry-footer -->

	<?php else : ?>

		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'coffeeshrub' ),
					get_the_title()
				),
				'<footer class="entry-footer"><span class="edit-link">',
				'</span></footer><!-- .entry-footer -->'
			);
		?>

	<?php endif; ?>
</article><!-- #post-## -->


