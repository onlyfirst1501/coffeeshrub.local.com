<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */
?>

<div class="news-item" id="post-<?php the_ID(); ?>" >
  <a href="<?php echo esc_url( get_permalink() ); ?>" class="img">
  	<?php echo get_the_post_thumbnail( get_the_ID(), 'archive-size' ); ?>
  </a>
  <div class="content">
    <h3 class="title"><a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title(); ?>"><?php the_title(); ?><?php if ( is_sticky() ) : ?>
      <span class="sticky-post">&nbsp;</span>
    <?php endif; ?></a></h3>
    <?php
    if ( has_excerpt()) : ?>
      <div class="excerpt">
        <?php the_excerpt(); ?>
      </div>
    <?php endif;
    ?>
  </div>
</div>
