<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage coffeeshrub
 * @since coffeeshrub 1.0
 */
/*
Template Name: Page Full width
*/

get_header(); ?>

<main id="main" class="site-main">
  <div class="container">
    <div class="primary-area">
      <?php  get_breadcrumb(); ?>
      <div class="contact-form">
      <?php
      // Start the loop.
      while ( have_posts() ) : the_post();

        // Include the page content template.
        get_template_part( 'template-parts/content', 'page' );
        // End of the loop.
      endwhile;
      ?>
      </div>
    </div><!-- .primary-area -->

  </div>
</main><!-- .site-main -->


<?php get_footer(); ?>
